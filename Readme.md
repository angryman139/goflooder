# Simple golang TCP flooder

Dumb & Simple TCP flooder for use in tests

# Warning
  - This software has no warranty at all
  - It's your responsibility

# Usage

### Edit following lines in main.go
```
connect, _ := net.Dial("tcp", "127.0.0.1:80")
message := "Sample text"
```

### Run flooder
``` sh
$ docker build -t goflooder .
$ docker run goflooder
