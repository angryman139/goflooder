FROM golang as builder
WORKDIR /root/app
COPY . .
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o=./flooder ./main.go

FROM alpine:3.12
WORKDIR /root/app
COPY --from=builder /root/app/flooder .
CMD ./flooder
